docker-compose up -d
DIR="app"
if [ ! -e "$DIR" ]; then
  docker exec php_mystories composer create-project --prefer-dist symfony/website-skeleton app
fi