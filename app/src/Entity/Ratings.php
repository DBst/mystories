<?php

namespace App\Entity;

use App\Repository\RatingsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RatingsRepository::class)]
class Ratings
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Stories::class, inversedBy: 'ratings')]
    #[ORM\JoinColumn(nullable: false)]
    private $story;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'ratings')]
    private $added_by;

    #[ORM\Column(type: 'string', length: 255)]
    private $ip_hash;

    #[ORM\Column(type: 'integer')]
    private $added_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStory(): ?Stories
    {
        return $this->story;
    }

    public function setStory(?Stories $story): self
    {
        $this->story = $story;

        return $this;
    }

    public function getAddedBy(): ?User
    {
        return $this->added_by;
    }

    public function setAddedBy(?User $added_by): self
    {
        $this->added_by = $added_by;

        return $this;
    }

    public function getIpHash(): ?string
    {
        return $this->ip_hash;
    }

    public function setIpHash(string $ip_hash): self
    {
        $this->ip_hash = $ip_hash;

        return $this;
    }

    public function getAddedAt(): ?int
    {
        return $this->added_at;
    }

    public function setAddedAt(int $added_at): self
    {
        $this->added_at = $added_at;

        return $this;
    }
}
