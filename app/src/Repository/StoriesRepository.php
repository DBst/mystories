<?php

namespace App\Repository;

use App\Entity\Stories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stories[]    findAll()
 * @method Stories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stories::class);
    }

    public function findMostPopularStories(int $limit, int $offset)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT *, s.added_at addedAt
            FROM stories s
            WHERE s.rating > (SELECT AVG(rating) FROM stories)
            ORDER BY addedAt DESC
        ';

        return $conn->prepare($sql)->executeQuery()->fetchAllAssociative();
    }

    public function findStories(int $limit, int $offset, int $status)
    {
        return $this->createQueryBuilder('s')
            ->where('s.status = ' . $status)
            ->orderBy('s.added_at', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->execute();
    }

    //Check if the selected ip hash has not exceeded the limit
    public function checkLimit(string $ip_hash, $limit = 5): bool
    {
        $count = $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->where('s.ip_hash = :ip_hash')
            ->setParameter('ip_hash', $ip_hash)
            ->andWhere('s.added_at > ' . (time() - 60*60))
            ->andWhere('s.status = 0')
            ->getQuery()
            ->execute()[0][1];

        if($count < $limit) {
            return true;
        }
        return false;
    }

    // /**
    //  * @return Stories[] Returns an array of Stories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stories
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
