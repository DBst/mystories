<?php

namespace App\Controller;

use App\Repository\StoriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(StoriesRepository $sr): Response
    {
        $stories = $sr->findStories(10, 0, 1);
        return $this->render('site/index.html.twig', [
            'stories' => $stories,
        ]);
    }

    #[Route('/waiting-room', name: 'waiting_room')]
    public function waitingRoom(StoriesRepository $sr): Response
    {
        $stories = $sr->findStories(10, 0, 0);
        return $this->render('site/index.html.twig', [
            'stories' => $stories,
        ]);
    }

    #[Route('/most-popular', name: 'most_popular')]
    public function mostPopular(StoriesRepository $sr): Response
    {
        $stories = $sr->findMostPopularStories(10, 0);
        return $this->render('site/index.html.twig', [
            'stories' => $stories,
        ]);
    }
}
