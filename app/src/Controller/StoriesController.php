<?php

namespace App\Controller;

use App\Entity\Ratings;
use App\Entity\Stories;
use App\Repository\RatingsRepository;
use App\Repository\StoriesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StoriesController extends AbstractController
{
    #[Route('/add-story', name: 'add_story')]
    public function index(): Response
    {
        return $this->render('stories/index.html.twig');
    }

    #[Route('/send-story', name: 'send_story')]
    public function sendStory(Request $request, ManagerRegistry $doctrine, StoriesRepository $sr): Response
    {
        try {
            if($request->isMethod('post')) {
                $ip_hash = hash('sha256', $request->getClientIp());
                if($sr->checkLimit($ip_hash)) {
                    $em = $doctrine->getManager();

                    $s = new Stories();
                    $s->setTitle(strip_tags($request->get('title')));
                    $s->setContent(strip_tags($request->get('title')));
                    if ($request->get('anonymous') !== '' && $this->getUser()) {
                        $s->setAddedBy($this->getUser());
                    }
                    $s->setAddedAt(time());
                    $s->setIpHash($ip_hash);
                    $s->setStatus(0);
                    $s->setRating(0);

                    $em->persist($s);
                    $em->flush();

                    return $this->json(['status' => true, 'message' => 'Story has successfully submitted.']);
                }
                return $this->json(['status' => false, 'message' => 'You cannot submit any more stories. Wait for a while and try again.']);
            }
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);

        } catch(\Exception $a) {
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        }
    }
    #[Route('/add-plus', name: 'add_plus')]
    public function addPlus(Request $request, ManagerRegistry $doctrine, RatingsRepository $rr, StoriesRepository $sr): Response
    {
        try {
            if($request->isMethod('post')) {
                $ip_hash = hash('sha256', $request->getClientIp());
                $id = (int) $request->get('id');
                $user = !empty($this->getUser()) ? (int) $this->getUser()->getUserIdentifier() : null;
                if($rr->checkLimit($ip_hash, $id, $user)) {
                    $story = $sr->findOneBy(['id' => $id]);
                    if(!empty($story)) {
                        $em = $doctrine->getManager();
                        $story->setRating($story->getRating()+1);
                        $em->persist($story);

                        $r = new Ratings();
                        $r->setIpHash($ip_hash);
                        if (!empty($this->getUser())) {
                            $r->setAddedBy($this->getUser());
                        }
                        $r->setStory($sr->findOneBy(['id' => $id]));
                        $r->setAddedAt(time());

                        $em->persist($r);
                        $em->flush();

                        return $this->json(['status' => true, 'message' => 'Story was liked successfully.']);
                    }
                }
                return $this->json(['status' => false, 'message' => 'You can\'t like this story a second time.']);
            }
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        } catch(\Exception $a) {
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        }
    }
}
