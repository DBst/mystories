<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Repository\CommentsRepository;
use App\Repository\StoriesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController extends AbstractController
{
    #[Route('/get-comments', name: 'get_comments')]
    public function getComments(Request $request, StoriesRepository $sr, CommentsRepository $cr): Response
    {
        try {
            if($request->isMethod('POST')) {
                $story_id = (int) $request->get('id');
                if(!empty($story_id)) {
                    $comments = $sr->findOneBy(['id' => $story_id])->getComments();
                    return $this->json(['status' => true, 'partial' => $this->render('comments/index.html.twig', [
                        'comments' => $comments,
                    ])->getContent()]);
                }
            }
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        } catch (\Exception $a) {
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        }
    }

    #[Route('/add-comment', name: 'add_comment')]
    public function addComment(Request $request, StoriesRepository $sr, CommentsRepository $cr, ManagerRegistry $doctrine): Response
    {
        try {
            if($request->isMethod('POST')) {
                $story_id = (int) $request->get('id');
                $content = strip_tags($request->get('content'));
                $story = $sr->findOneBy(['id' => $story_id]);
                if(!empty($story)) {
                    if(!empty($content)) {
                        $em = $doctrine->getManager();

                        $c = new Comments();
                        $c->setStory($story);
                        $c->setAddedAt(time());
                        $c->setAddedBy($this->getUser());
                        $c->setContent($content);

                        $em->persist($c);
                        $em->flush();

                        return $this->json(['status' => true, 'message' => 'Comment successfully added.']);
                    }
                    return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
                }
            }
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        } catch (\Exception $a) {
            return $this->json(['status' => false, 'message' => 'A problem occured! Please try again in a moment.']);
        }
    }
}
