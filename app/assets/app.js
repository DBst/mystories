/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import $ from 'jquery';

import * as mdb from 'mdb-ui-kit'; // lib
import 'mdb-ui-kit/css/mdb.min.css';
import "@fortawesome/fontawesome-free/js/all";
import Notiflix from 'notiflix';

// const app = (function() {

    function loader(status = true) {
        if(status) {
            Notiflix.Loading.standard();
        } else {
            Notiflix.Loading.remove();
        }
    }

    function message(status = true, message) {
        if(status) {
            Notiflix.Notify.success(message);
        } else {
            Notiflix.Notify.failure(message);
        }
    }

    function sendAjax(data = {
        'url': '',
        'data': {},
    }, options = {
        'method': 'POST',
        'type': 'JSON',
    }) {
        return $.ajax({
            url: data.url,
            data: data.data,
            method: options.method,
            type: options.type,
            error: function(e) {
                loader(false);
            }
        });
    }

    $(document).on('submit', '#add-story-form', function(e) {
        e.preventDefault();
        loader();
        let submit_ = this;
        sendAjax({
            'data': $(this).serializeArray(),
            'url': $(this).attr('action'),
        }).then(function(e) {
            if(e.status) {
                $(submit_).find('input, textarea').val('');
            }
            message(e.status, e.message);
            loader(false);
        })

    })

    //add a button Show More for stories with more than 6 text line
    $.each($('.story-content'), function(i, item) {
        let lines = $(item).outerHeight() / parseInt($(item).css('lineHeight'));
        if(lines > 6) {
            $(item).closest('.story-card').find('.story-buttons .col-md-4.text-center').append(`<a href="javascript:void(0)" class="show-more">Show more</a>`);
        }
    });

    $(document).on('click', '.show-more', function(e) {
        $(this).closest('.col-md-12').find('.story-content').addClass('full-story-content');
        $(this).text('Show comments').removeClass('show-more').addClass('show-comments');
    });

    $(document).on('click', '.add-plus', function(e) {
        let click_ = this;
        let id = $(this).closest('.story-card').attr('story-id');
        loader();
        sendAjax({
            'url': '/add-plus',
            'data': {'id': id}
        }).then(function(e) {
            loader(false);
            if(e.status) {
                let count = $(click_).parent().find('span');
                $(count).text(parseInt($(count).text())+1);
            }
            message(e.status, e.message);
        });
    });

    $(document).on('click', '.show-comments', function(e) {
        $(this).text('Hide comments');
        $(this).removeClass('show-comments').addClass('hide-comments');
        getComments(this);
    });

    function getComments(item) {
        let id = $(item).closest('.story-card').attr('story-id');
        console.log(id)
        loader();
        sendAjax({
            'url': '/get-comments',
            'data': {'id': id}
        }).then(function(e) {
            loader(false);
            if(e.status) {
                let commentsContent = $(item).closest('.story-buttons').find('.comments-list').html(e.partial);
                $(item).closest('.story-buttons').find('.comment-section').removeClass('d-none');
            } else {
                message(e.status, e.message);
            }
        })
    }

    $(document).on('submit', '.add-comment', function(e) {
        e.preventDefault();
        let submit_ = this;
        let id = $(this).closest('.story-card').attr('story-id');
        let content = $(this).find('#content').val();
        loader();
        sendAjax({
            'url': '/add-comment',
            'data': {
                'id': id,
                'content': content
            }
        }).then(function(e) {
            loader(false);
            if(e.status) {
                $(submit_).find('#content').val('');
                getComments(submit_);
                console.log(e)
            }
            message(e.status, e.message);
        })

    })

    $(document).on('hide-comments', function(e) {

    })

// })();