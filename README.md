## Trojansky - MyStories

MyStories is a simple website where users can post their stories.

Website allows you to upload your own stories, rate stories and publish comments. 

Created with:
- Docker
- Symfony
- jQuery
- Bootstrap 5 + MDBootstrap
- and some other libraries 
